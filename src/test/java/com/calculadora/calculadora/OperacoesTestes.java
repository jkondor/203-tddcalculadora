package com.calculadora.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTestes {

    //Soma
    @Test
    public void testarOperacaoDeSoma(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);

        Assertions.assertEquals(Operacao.soma(numeros),6);
    }

    @Test
    public void testarOperacaoDeSomaDeDoisNumeros(){
        Assertions.assertEquals(Operacao.soma(1,1),2);
    }

    //Subtração
    @Test
    public void testarOperacaoDeSubtracaoDeDoisNumeros(){
        Assertions.assertEquals(Operacao.subtrai(3,2),1);
    }

    @Test
    public void testarOperacaoDeSubtracao(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(10);
        numeros.add(3);
        numeros.add(2);

        Assertions.assertEquals(Operacao.subtrai(numeros),5);
    }

    //Multiplicacao
    @Test
    public void testarOperacaoDeMultiplicacaoDeDoisNumeros(){
        Assertions.assertEquals(Operacao.multiplica(3,2),6);
    }

    @Test
    public void testarOperacaoDeMultiplicacao(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(3);
        numeros.add(2);

        Assertions.assertEquals(Operacao.multiplica(numeros),12);
    }

    //Divisao
    @Test
    public void testarOperacaoDeDivisao(){
        Assertions.assertEquals(Operacao.divide(6,2),3);
    }

    @Test
    public void testarValorInvalidoNaOperacaoDeDivisao(){
       // Assertions.assertEquals(Operacao.divide(3,0),3);
        Assertions.assertThrows(ArithmeticException.class, () -> {Operacao.divide(3,0);});
    }
}