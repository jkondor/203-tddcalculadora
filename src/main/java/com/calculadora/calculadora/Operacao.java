package com.calculadora.calculadora;

import ch.qos.logback.core.rolling.helper.IntegerTokenConverter;

import java.util.List;

public class Operacao {

    public static int soma(int num1, int num2){
        //Primeira versão para funcionar.
        // return 2;
        int resultado = num1 + num2;
        return resultado;
    }

    public static int soma(List<Integer> numeros ){
        int resultado = 0;

        for (Integer numero: numeros){
            resultado += numero;
        }
        return resultado;
    }

    public static int subtrai(int num1, int num2){
        //return 1;
        int resultado = num1 - num2;
        return resultado;
    }

    public static int subtrai(List<Integer> numeros) {
        //return 5;
        int resultado = 0;

        for (int numero: numeros) {
            if (numeros.indexOf(numero) == 0) {
                resultado = numero;
            } else {
                resultado -= numero;
            }
        }
        return resultado;
    }

    public static int multiplica(int num1, int num2) {
        //return 6;
        int resultado = num1 * num2;
        return resultado;
    }

    public static int multiplica(List<Integer> numeros) {
        //return 12;

        int resultado = 1;
        for (int numero: numeros){
            resultado *= numero;
        }
        return resultado;
    }

    public static int divide(int num1, int num2) {
        //return 3;
        int resultado = num1 / num2;
        return resultado;
    }
}
